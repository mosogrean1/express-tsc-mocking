import { gql } from 'apollo-server-express';

const typeDefs = gql`
  type Query {
    testquery(text: String!): String!
  }
  type Mutation {
    testmutaion(input: String!): String!
  }
`;

export default typeDefs;
