import { Request } from 'express';
import { ITestQuery } from '../Request/TestRequest';

class TestQuery {
  public async testquery(
    _: unknown,
    args: ITestQuery,
    { res }: { res: Request }
  ): Promise<string> {
    const { text } = args;
    return text;
  }
}

export default new TestQuery();
