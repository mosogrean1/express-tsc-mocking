import * as yup from 'yup';

export interface IReqHeaderRequest {
  text: string;
}

export const ReqHeaderRequest = yup.object().shape<IReqHeaderRequest>({
  text: yup.string().required(),
});
