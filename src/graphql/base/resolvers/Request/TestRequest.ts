import * as yup from 'yup';
export interface ITestQuery {
  text: string;
}

export const testQueryRequest = yup.object().shape<ITestQuery>({
  text: yup.string().required(),
});

export interface ITestMutation {
  input: string;
}

export const testMutationRequest = yup.object().shape<ITestMutation>({
  input: yup.string().required(),
});
