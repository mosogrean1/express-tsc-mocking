import { Request, Response } from 'express';
import { ITestMutation } from '../Request/TestRequest';

class TestMutation {
  public async testmutation(
    _: unknown,
    { input }: ITestMutation,
    { req }: { req: Request }
  ): Promise<string> {
    return input;
  }
}

export default new TestMutation();
