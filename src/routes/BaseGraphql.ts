import { formatGraphqlError } from '@shared/ErrorHandle';
import { ApolloServer } from 'apollo-server-express';
import typeDefs from '../graphql/base/typeDefs';
import resolvers from '../graphql/base/resolvers/index';

const BaseApolloServer = new ApolloServer({
  resolvers,
  typeDefs,
  context: async ({ req }: { req: Request }): Promise<{ req: Request }> => {
    return { req };
  },
  tracing: true,
  playground: process.env.GRAPHQL_PLAYGROUND === 'true',
  formatError: formatGraphqlError,
});

export default BaseApolloServer;
