import ErrorHandler from './ErrorHandle';
import { BAD_REQUEST } from 'http-status-codes';

class ValidateHandle {
  args: any;
  schema: any;

  constructor(args: any, schema: any) {
    this.args = args;
    this.schema = schema;
  }

  public async graphql(): Promise<void> {
    try {
      await this.schema.validate(this.args, { abortEarly: false });
    } catch (error) {
      throw new ErrorHandler(BAD_REQUEST, 'BAD_REQUEST', error).graph();
    }
  }
}

export default ValidateHandle;
