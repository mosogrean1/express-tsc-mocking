import logger from '@shared/Logger';
import { ApolloError } from 'apollo-server-express';

class ErrorHandler extends Error {
  statusCode: number;
  message: string;
  option: object | undefined;

  constructor(statusCode: number, message?: string, option?: object) {
    super();
    this.statusCode = statusCode;
    this.message = message || 'There are no recommendations for this incident.';
    this.option = option;
  }

  res(res: any): void {
    res.status(this.statusCode).json({
      code: this.statusCode,
      message: this.message,
      option: this.option || null,
    });
  }

  graph(): void {
    logger.error(
      `GRAPH code: ${this.statusCode.toString()} message: ${
        this.message
      } option: ${JSON.stringify(this.option)}`
    );
    throw new ApolloError(
      this.message,
      this.statusCode.toString(),
      this.option
    );
  }
}

export const formatGraphqlError = (err: any): Error => {
  err.message = err.message.replace('Context creation failed: ', '');
  delete err.extensions.exception;
  return err;
};

export default ErrorHandler;
