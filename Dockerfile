FROM node:13.14.0-alpine

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY . /usr/src/app
RUN yarn install
RUN yarn run build


CMD [ "yarn", "run", "start" ]

# docker build -f Dockerfile -t name:01 .
# docker run -p 3000:8081 -d name:01